/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Singletons;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import users.Address;
import users.Administrator;
import utilities.Serialiser;

/**
 *
 * @author Goel
 */
public class AdministratorsTest {
    
    private Administrators admins;
    
    private Address testAddress;
    private Administrator testAdmin;
      
    
    public AdministratorsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        admins = Administrators.getInstance();
        
        // Use save data to create the file initially.
        //admins.saveData();
        
        // Read data from the instance.
        admins.readData();
        
        testAddress = new Address("01 Plymouth Road", "Plymouth", "PL1 1KN");
    }
    
    @After
    public void tearDown() {
 
    }
    
    
    @Test
    public void testCreateAdministratorAccount() {
        testAdmin = new Administrator("A0001", "testPass", "Joe", "Logs", testAddress);
        
        // Create the account.
        assertTrue("Create Admin", admins.createAdministratorAccount(testAdmin));
        
        System.out.println("Created admin info:");
        admins.getAdministrators().forEach((administrator) -> {
           System.out.println(administrator.getID());
           System.out.println(administrator.getFirstName());
           System.out.println(administrator.getLastName());
           System.out.println(administrator.getNotifications());
        });
        
    }
}
