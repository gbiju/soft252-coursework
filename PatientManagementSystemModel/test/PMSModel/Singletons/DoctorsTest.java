/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Singletons;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import users.Address;
import users.Administrator;
import users.Doctor;
import users.Patient;
import utilities.AdminFeedback;
import utilities.PatientFeedback;




/**
 *
 * @author Goel
 */
public class DoctorsTest {
    
    private Administrators admins;
    private Doctors doctors;
    
    private Address testAddress;
    
    private Doctor testDoctorOne;
    private Doctor testDoctorTwo;
    private Doctor testDoctorThree;
    
    public DoctorsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        admins = Administrators.getInstance();
        doctors = Doctors.getInstance();
        
        admins.readData();
        doctors.readData();
        
        testAddress = new Address("02 Plymouth Road", "Plymouth", "PL2 2KN");
        
        testDoctorOne = new Doctor("D0001", "testPass", "Joe", "Logs", testAddress);
        testDoctorTwo = new Doctor("D0002", "testPass", "Julian", "Mathes", testAddress);
        testDoctorThree = new Doctor("D0003", "testPass", "Andy", "Carthew", testAddress);
    }
    
    @After
    public void tearDown() {
    }
    
    
    @Test
    public void testCreateDoctorAccount() {
        System.out.println("\nTesting creating a doctor account.");
        
        // Create an admin who is going to add the doctor.
        Administrator testAdmin = new Administrator("A0001", "testPass", "Joe", "Logs", testAddress);
        
        assertTrue("Create Doctor Two", doctors.createDoctorAccount(testAdmin, testDoctorTwo));
        assertTrue("Create Doctor Three", doctors.createDoctorAccount(testAdmin, testDoctorThree));
        
        System.out.println("Created doctors info:");
        doctors.getDoctors().forEach((doctor) -> {
           System.out.println(doctor.getID());
           System.out.println(doctor.getFirstName());
           System.out.println(doctor.getLastName());
           System.out.println(doctor.getNotifications());
        });
        
        System.out.println("Administrators notifications.");
        admins.getAdministrators().forEach((admin) -> {
                System.out.println(admin.getNotifications());
        });
    }
    
    
    @Test
    public void testRateDoctor() {
        System.out.println("Testing rating a doctor.");
        
        // Create an admin who is going to add the doctor.
        Administrator testAdmin = new Administrator("A0001", "testPass", "Joe", "Logs", testAddress);
        
        assertTrue("Create Doctor One", doctors.createDoctorAccount(testAdmin, testDoctorOne));
        
        // Create test patient to rate doctor.
        Patient testPatient = new Patient("P0001", "testPass", "Goel", "Biju", testAddress, 18, LocalDate.of(2000, 3, 1), "Male");
        
        PatientFeedback feedback = new PatientFeedback(testPatient, LocalDateTime.now(), "Brilliant doctor", 8);
        
        assertTrue("Rate Doctor", doctors.rateDoctor(testDoctorOne, feedback));
        System.out.println("Rated Doctor");

        System.out.println("\n\nPatient Feedback for doctors:");
        doctors.getDoctors().forEach((doctor) -> {
            System.out.print(doctor.getPatientFeedbacks());
        });
    }
    
    
    @Test
    public void testRemoveDoctorAccount() {
        System.out.println("Testing removing a doctor.");
        
        // Create an admin who is going to add the doctor.
        Administrator testAdmin = new Administrator("A0001", "testPass", "Joe", "Logs", testAddress);
    
        assertTrue("Create Doctor One", doctors.createDoctorAccount(testAdmin, testDoctorOne));
        
        System.out.println("Length of doctors list: " + doctors.getDoctors().size());
        assertTrue("Remove Doctor", doctors.removeDoctorAccount(testAdmin, testDoctorOne));
        System.out.println("Removed doctor, length of doctors list: " + doctors.getDoctors().size());
    }
    
    
    @Test
    public void testFeedbackToDoctor() {
        System.out.println("Testing feedback to doctor from Administrator.");
        
        // Create an admin who is going to add the doctor.
        Administrator testAdmin = new Administrator("A0001", "testPass", "Joe", "Logs", testAddress);
        
        // Create another doctor to test.
        assertTrue("Create Doctor One", doctors.createDoctorAccount(testAdmin, testDoctorOne));
        
        AdminFeedback feedback = new AdminFeedback(testAdmin, LocalDateTime.now(), "Great Doctor! Patients excited to see you.");
        
        assertTrue("Admin feedback to Doctor", doctors.feedbackToDoctor(testDoctorOne, feedback));
        
        System.out.println("\n\nAdmin Feedback for doctors:");
        doctors.getDoctors().forEach((doctor) -> {
            System.out.println(doctor.getAdminFeedbacks());
        });
    }
}
