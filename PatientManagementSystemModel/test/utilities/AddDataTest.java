/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import PMSModel.Singletons.Administrators;
import PMSModel.Singletons.Doctors;
import PMSModel.Singletons.Patients;
import PMSModel.Singletons.Secretaries;
import java.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import users.Address;
import users.Administrator;
import users.Doctor;
import users.Patient;
import users.Secretary;

/**
 *
 * @author Goel
 */
public class AddDataTest {
    
    private Administrators admins;
    private Doctors doctors;
    private Patients patients;
    private Secretaries secretaries;
    
    private Administrator testAdmin;
    
    private Patient testPatientOne;
    private Patient testPatientTwo;
    private Patient testPatientThree;
    
    private Doctor testDoctorOne;
    private Doctor testDoctorTwo;
    private Doctor testDoctorThree;
    
    private Secretary testSecretary;
    
    
    public AddDataTest() {
    }
    
     @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Address testAddress = new Address("02 Plymouth Road", "Plymouth", "PL2 2KN");
        
        // 1 Admin
        testAdmin = new Administrator("A0001", "testPass", "Joe", "Logs", testAddress);
        
        // 3 Patients
        // Create test patient to rate doctor.
        testPatientOne = new Patient("P0001", "testPass", "Goel", "Biju", testAddress, 18, LocalDate.of(2000, 3, 1), "Male");
        testPatientTwo = new Patient("P0002", "testPass", "Roman", "Blinky", testAddress, 32, LocalDate.of(1988, 3, 1), "Male");
        testPatientThree = new Patient("P0003", "testPass", "Steven", "Spear", testAddress, 55, LocalDate.of(1965, 3, 1), "Male");
        
        // 3 Doctors
        testDoctorOne = new Doctor("D0001", "testPass", "Joe", "Logs", testAddress);
        testDoctorTwo = new Doctor("D0002", "testPass", "Julian", "Mathes", testAddress);
        testDoctorThree = new Doctor("D0003", "testPass", "Andy", "Carthew", testAddress);
        
        testSecretary = new Secretary("S0001", "testPass", "Julian", "Mathews", testAddress);
        
        
        // Read and get data.
        admins = Administrators.getInstance();
        admins.readData();
        
        doctors = Doctors.getInstance();
        doctors.readData();
        
        patients = Patients.getInstance();
        patients.readData();
        
        secretaries = Secretaries.getInstance();
        secretaries.readData();
    }
    
    @After
    public void tearDown() {
    }
    
    @Test 
    public void addUsers() {
        
        // Add admins.
        System.out.println("\n\nAdd Administrators.");
        admins.createAdministratorAccount(testAdmin);
        
        System.out.println("\n\nAdministrators:");
        System.out.println(admins.getAdministrators());
        
        // Add secretaries.
        System.out.println("\n\nAdd Secretaries.");
        secretaries.createSecretaryAccount(testAdmin, testSecretary);
        
        System.out.println("\n\nSecretaries:");
        System.out.println(secretaries.getSecretaries());
        
        // Add patients.
        System.out.println("\n\nAdd Patients.");
        patients.requestPatientAccount(testPatientOne);
        patients.requestPatientAccount(testPatientTwo);
        patients.requestPatientAccount(testPatientThree);
        
        System.out.println("\n\nPatients:");
        System.out.println(patients.getPatients());
        
        // Add Doctors.
        System.out.println("\n\nAdd Doctors.");
        doctors.createDoctorAccount(testAdmin, testDoctorOne);
        System.out.println(doctors.getNextID("D"));
        doctors.createDoctorAccount(testAdmin, testDoctorTwo);
        System.out.println(doctors.getNextID("D"));
        doctors.createDoctorAccount(testAdmin, testDoctorThree);
        
        System.out.println("\n\nDoctors:");
        System.out.println(doctors.getDoctors());
        
       
        System.out.println(doctors.getNextID("D"));
    }
}
