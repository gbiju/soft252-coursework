/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import PMSModel.Singletons.Administrators;
import PMSModel.Singletons.Doctors;
import PMSModel.Singletons.Patients;
import PMSModel.Singletons.Secretaries;
import java.io.File;
import java.time.LocalDate;
import java.time.Month;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import users.Address;
import users.Administrator;
import users.Doctor;
import users.Patient;
import users.Secretary;

/**
 *
 * @author Goel
 */
public class SerialiserTest {
    
    private Serialiser adminSerialiser;
    private Serialiser patientSerialiser; 
    private Serialiser doctorSerialiser; 
    private Serialiser secretarySerialiser; 
    
    private Patient testPatient;
    private Doctor testDoctor;
    private Secretary testSecretary;
    private Administrator testAdmin;
    
    private LocalDate testDate;
    private Address testAddress;
    
    
    public SerialiserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        // Create a new serialiser objects to test.
        adminSerialiser = new Serialiser("data//administrators.ser");
        
        testDate = LocalDate.of(2000, 1, 1);
        testAddress = new Address("01 Plymouth Road", "Plymouth", "PL1 1KN");
        
        testAdmin = new Administrator("A0001", "testPass", "Joe", "Logs", testAddress);
    }
    
    
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testGetFileName() {
        System.out.println("Testing get file name.");
        
        assertEquals("File Name", "data//administrators.ser", adminSerialiser.getFileName());
    }
    
    
    @Test
    public void testSetFileName() {
        System.out.println("Testing set file name.");
       
        adminSerialiser.setFileName("data//test_administrator.ser");
        assertEquals("File Name", "data//test_administrator.ser", adminSerialiser.getFileName());
    }
    
    @Test
    public void testWriteObject() {
        System.out.println("Testing writing an object.");
        
        // Populate with one admin.
        assertTrue("Serialised Admin", adminSerialiser.writeObject(testAdmin));
    }
    
    @Test
    public void testReadObject() {
        System.out.println("Testing reading from file.");
        
        Administrator readAdmin = (Administrator) adminSerialiser.readObject();
        assertEquals("Read Admin", testAdmin.getID(), readAdmin.getID());
    }
    
}
