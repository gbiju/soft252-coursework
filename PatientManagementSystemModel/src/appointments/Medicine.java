/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointments;

import PMSModel.Observables.DataObservable;
import java.io.Serializable;

/**
 *
 * @author Goel
 */
public class Medicine implements Serializable {
    
    private String name;
    private Integer quantityInStock;
    private Integer quantityToBuy;
    
    /**
     * 
     * @param name 
     * @param qtyBuy 
     */
    public Medicine(String name, Integer qtyBuy) {
        this.name = name;
        this.quantityToBuy = qtyBuy;
        
        this.quantityInStock = 0;
    }
    
    
    /**
     * 
     * @param name
     * @param qtyStock
     * @param qtyBuy
     */
    public Medicine(String name, Integer qtyStock, Integer qtyBuy) {
        this.name = name;
        this.quantityInStock = qtyStock;
        this.quantityToBuy = qtyBuy;
    }

    /**
     * 
     * @return 
     */
    public String getName() {
        return name;
    }

    
    /**
     * 
     * @param name 
     */
    public void setName(String name) {
        if (!name.isEmpty()) {
            this.name = name;
        }
    }

    
    /**
     * 
     * @return 
     */
    public Integer getQuantityInStock() {
        return quantityInStock;
    }

    
    /**
     * 
     * @param newQty 
     */
    public void setQuantityInStock(Integer newQty) {
        if (newQty > -1) {
            this.quantityInStock = newQty;
        }
    }

    
    /**
     * 
     * @return 
     */
    public Integer getQuantityToBuy() {
        return quantityToBuy;
    }

    
    /**
     * 
     * @param quantityToBuy 
     */
    public void setQuantityToBuy(Integer quantityToBuy) {
        if (quantityToBuy > 0) {
            this.quantityToBuy = quantityToBuy;
        }   
    }
}
