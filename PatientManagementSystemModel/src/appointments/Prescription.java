/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointments;

import PMSModel.Observables.DataObservable;
import java.io.Serializable;
import users.Patient;

/**
 *
 * @author Goel
 */
public class Prescription implements Serializable {
    
    private Patient patient;
    private Medicine medicine;
    private Integer prescriptionQuantity;
    private String dosage;
    private Boolean collectedPrescription;
    
    /**
     * 
     * @param patient
     * @param medicine
     * @param quantity
     * @param dosage 
     */
    public Prescription(Patient patient, Medicine medicine, Integer quantity, String dosage) {
        this.patient = patient;
        this.medicine = medicine;
        this.prescriptionQuantity = quantity;
        this.dosage = dosage;
        this.collectedPrescription = false;
    }

    /**
     *
     * @return
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     *
     * @param patient
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     *
     * @return
     */
    public Medicine getMedicine() {
        return medicine;
    }

    /**
     *
     * @param medicine
     */
    public void setMedicine(Medicine medicine) {
        if (medicine != null) {
            this.medicine = medicine;
        }
    }

    /**
     *
     * @return
     */
    public Integer getPrescriptionQuantity() {
        return prescriptionQuantity;
    }

    /**
     *
     * @param prescriptionQuantity
     */
    public void setPrescriptionQuantity(Integer prescriptionQuantity) {
        if (prescriptionQuantity > 0) {
            this.prescriptionQuantity = prescriptionQuantity;
        }   
    }

    /**
     *
     * @return
     */
    public String getDosage() {
        return dosage;
    }

    /**
     *
     * @param dosage
     */
    public void setDosage(String dosage) {
        if (!dosage.isEmpty()) {
            this.dosage = dosage;
        }
    }

    /**
     *
     * @return
     */
    public Boolean getCollectedPrescription() {
        return collectedPrescription;
    }

    /**
     *
     * @param collectedPrescription
     */
    public void setCollectedPrescription(Boolean collectedPrescription) {
        this.collectedPrescription = collectedPrescription;
    }
    
    
}
