/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointments;

import PMSModel.Singletons.Appointments;
import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import users.Doctor;
import users.Patient;
import PMSModel.Observables.DataObservable;
import java.util.Objects;


/**
 *
 * @author Goel
 */
public class Appointment extends DataObservable implements Comparable<Appointment>, Serializable {
    
    private Patient patient;
    private Doctor doctor;
    
    private LocalDateTime createdTime;
    
    // Include requests with potential dates.
    private ArrayList<LocalDateTime> requestedDateTimes;
    private LocalDateTime confirmedDateTime;
    
    private String appointmentNotes;
    private ArrayList<Prescription> appointmentPrescriptions;
    
    private Boolean confirmedAppointment;
    private Boolean completedAppointment;
  
    
    /**
     * 
     * @param patient
     * @param requestedDoctor
     * @param requestedTimes 
     */
    public Appointment(Patient patient, Doctor requestedDoctor, ArrayList<LocalDateTime> requestedTimes) {
        this.patient = patient;
        this.doctor = requestedDoctor;
        
        this.requestedDateTimes = requestedTimes;
        
        this.confirmedAppointment = false;
        this.completedAppointment = false;
        
        this.createdTime = LocalDateTime.now();
        this.appointmentNotes = new String();
        this.appointmentPrescriptions = new ArrayList<>();
    }
    
    
    /**
     * 
     * @param patient
     * @param doctor
     * @param bookedDateTime 
     */
    public Appointment(Patient patient, Doctor doctor, LocalDateTime bookedDateTime) {
        this.patient = patient;
        this.doctor = doctor;
        this.confirmedDateTime = bookedDateTime;
        this.confirmedAppointment = true;
        this.completedAppointment = false;
        this.createdTime = LocalDateTime.now();
        this.appointmentNotes = new String();
        this.appointmentPrescriptions = new ArrayList<>();
    }

    /**
     *
     * @return
     */
    public LocalDateTime getCreatedTime() {
        return createdTime;
    }
    
    /**
     * 
     * @return 
     */
    public Patient getPatient() {
        return patient;
    }

    
    /**
     * 
     * @param patient 
     */
    public void setPatient(Patient patient) {
        if (patient != null) {
            this.patient = patient;
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    public Doctor getDoctor() {
        return doctor;
    }

    
    /**
     * 
     * @param doctor 
     */
    public void setDoctor(Doctor doctor) {
        if (doctor != null) {
            this.doctor = doctor;
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    public ArrayList<LocalDateTime> getRequestedDateTimes() {
        return requestedDateTimes;
    }

    
    /**
     * 
     * @param requestedDateTimes 
     */
    public void setRequestedDateTimes(ArrayList<LocalDateTime> requestedDateTimes) {
        if (requestedDateTimes != null && !requestedDateTimes.isEmpty()) {
            this.requestedDateTimes = requestedDateTimes;
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    public LocalDateTime getConfirmedDateTime() {
        return confirmedDateTime;
    }

    
    /**
     * 
     * @param confirmedDateTime 
     */
    public void setConfirmedDateTime(LocalDateTime confirmedDateTime) {
        if (confirmedDateTime != null) {
            this.confirmedDateTime = confirmedDateTime;
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    //public Duration getAppointmentDuration() {
    //    return appointmentDuration;
    //}

    
    /**
     * 
     * @param appointmentDuration 
     */
    //public void setAppointmentDuration(Duration appointmentDuration) {
    //    if (appointmentDuration != null) {
    //        this.appointmentDuration = appointmentDuration;
    //        this.notifyObservers();
    //    }
    //}

    
    /**
     * 
     * @return 
     */
    public String getAppointmentNotes() {
        return appointmentNotes;
    }

    
    /**
     * 
     * @param appointmentNotes 
     */
    public void setAppointmentNotes(String appointmentNotes) {
        if (!appointmentNotes.isEmpty()) {
            this.appointmentNotes = appointmentNotes;
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    public ArrayList<Prescription> getAppointmentPrescriptions() {
        return appointmentPrescriptions;
    }

    
    /**
     * 
     * @param appointmentPrescriptions 
     */
    public void setAppointmentPrescriptions(ArrayList<Prescription> appointmentPrescriptions) {
        if (!appointmentPrescriptions.isEmpty()) {
            this.appointmentPrescriptions = appointmentPrescriptions;
            this.notifyObservers();
        }
    }
    
    
    /**
     * 
     * @param newPrescription 
     */
    public void addAppointmentPrescription(Prescription newPrescription) {
        if (newPrescription != null) {
            this.appointmentPrescriptions.add(newPrescription);
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    public Boolean getConfirmedAppointment() {
        return confirmedAppointment;
    }

    
    /**
     * 
     * @param confirmedAppointment 
     */
    public void setConfirmedAppointment(Boolean confirmedAppointment) {
        this.confirmedAppointment = confirmedAppointment;
        this.notifyObservers();
    }

    
    /**
     * 
     * @return 
     */
    public Boolean getCompletedAppointment() {
        return completedAppointment;
    }

    
    /**
     * 
     * @param completedAppointment 
     */
    public void setCompletedAppointment(Boolean completedAppointment) {
        this.completedAppointment = completedAppointment;
        this.notifyObservers();
    }
    
    
    /**
     * 
     * @param t
     * @return 
     */
    @Override
    public int compareTo(Appointment t) {
        int result = 0;
        LocalDateTime tTime = t.getConfirmedDateTime();
        if (confirmedDateTime != null) {
            if (!confirmedDateTime.equals(tTime)) {
                if (confirmedDateTime.isBefore(tTime)) {
                    result = -1;
                } else {
                    result = 1;
                }
            }
        }
        return result;
    }
    
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof Appointment) {
            Appointment target = (Appointment)o;
            if (0 == this.compareTo(target)) {
                if (this.appointmentNotes.equals(target.getAppointmentNotes())) {
                    if (this.appointmentPrescriptions.equals(target.getAppointmentPrescriptions())) {
                        if (this.doctor.equals(target.getDoctor())) {
                            if (this.patient.equals(target.getPatient())) {
                                if (this.completedAppointment.equals(target.getCompletedAppointment())) {
                                    if (this.confirmedAppointment.equals(target.getConfirmedAppointment())) {
                                        if (this.confirmedDateTime.equals(target.getConfirmedDateTime())) {
                                            if (this.requestedDateTimes.equals(target.getRequestedDateTimes())) {
                                                result = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.patient);
        hash = 53 * hash + Objects.hashCode(this.doctor);
        hash = 53 * hash + Objects.hashCode(this.requestedDateTimes);
        hash = 53 * hash + Objects.hashCode(this.confirmedDateTime);
        hash = 53 * hash + Objects.hashCode(this.appointmentNotes);
        hash = 53 * hash + Objects.hashCode(this.appointmentPrescriptions);
        hash = 53 * hash + Objects.hashCode(this.confirmedAppointment);
        hash = 53 * hash + Objects.hashCode(this.completedAppointment);
        return hash;
    }
    
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        
        if (!this.confirmedAppointment && !this.completedAppointment) {
            result.append("[Requested on ");
            result.append(this.createdTime.format(format));
            result.append("] Appointment");
        } else if (this.confirmedAppointment && !this.completedAppointment) {
            result.append("[Confirmed for - ");
            result.append(this.confirmedDateTime.format(format));
            result.append("] Appointment");
        } else {
            result.append("[Completed] Appointment");
        }
        
        if (this.patient != null) {
            result.append(" - Patient (");
            result.append(this.patient.getID());
            result.append(")");
        }
        
        return result.toString();
    }
}
