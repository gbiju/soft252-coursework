/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package users;

import java.time.LocalDate;
import java.time.Period;

/**
 *
 * @author Goel
 */
public class Patient extends User {
    
    private Integer age;
    private LocalDate dateOfBirth;
    private String gender;
    
    private Boolean accountApproved;
    private Boolean requestedTermination;
    
    
    /**
     * 
     */
    public Patient() {
        super();
        
        this.age = -1;
        this.dateOfBirth = null;
        this.gender = "UNKNOWN";
        this.accountApproved = null;
        this.requestedTermination = null;
        
        this.notifyObservers();
    }
    
    
    /**
     * 
     * @param ID
     * @param password
     * @param firstName
     * @param lastName
     * @param address 
     */
    public Patient(String ID, String password, String firstName, String lastName, Address address) {
        
        super(ID, password, firstName, lastName, address);
        
        this.accountApproved = false;
        this.requestedTermination = false;
        
        this.notifyObservers();
    }
    
    /**
     *
     * @param ID
     * @param password
     * @param firstName
     * @param lastName
     * @param address
     * @param age
     * @param dateOfBirth
     * @param gender
     */
    public Patient(String ID, String password, String firstName, String lastName, Address address,
            Integer age, LocalDate dateOfBirth, String gender) {
        super(ID, password, firstName, lastName, address);
        
        this.age = age;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        
        this.accountApproved = false;
        this.requestedTermination = false;
        
        this.notifyObservers();
    }
    
    
    /**
     * 
     * @return 
     */
    @Override
    public UserType getUserType() {
        return UserType.PATIENT;
    }

    
    /**
     * 
     * @return 
     */
    public Integer getAge() {
        return age;
    }

    
    /**
     * 
     * @return 
     */
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    
    /**
     * 
     * @param dateOfBirth 
     */
    public void setDateOfBirth(LocalDate dateOfBirth) {
        if (dateOfBirth != null) {
            this.dateOfBirth = dateOfBirth;
            
            // Calculate and set the age of the Patient.
            LocalDate today = LocalDate.now();
            this.age = Period.between(dateOfBirth, today).getYears();
            
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    public String getGender() {
        return gender;
    }

    
    /**
     * 
     * @param gender 
     */
    public void setGender(String gender) {
        if (!gender.isEmpty()) {
            this.gender = gender;    
            
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    public Boolean getAccountApproved() {
        return accountApproved;
    }

    
    /**
     * 
     * @param accountApproved 
     */
    public void setAccountApproved(Boolean accountApproved) {
        this.accountApproved = accountApproved;
        
        this.notifyObservers();
    }

    
    /**
     * 
     * @return 
     */
    public Boolean getRequestedTermination() {
        return requestedTermination;
    }

    
    /**
     * 
     * @param requestedTermination 
     */
    public void setRequestedTermination(Boolean requestedTermination) {
        this.requestedTermination = requestedTermination;
        
        this.notifyObservers();
    }
    
    
}
