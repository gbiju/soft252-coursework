/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package users;

import java.io.Serializable;

/**
 *
 * @author Goel
 */
public enum UserType implements Serializable {
    
    /**
     *
     */
    PATIENT,

    /**
     *
     */
    ADMINISTRATOR,

    /**
     *
     */
    SECRETARY,

    /**
     *
     */
    DOCTOR
}
