/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package users;

import PMSModel.Observables.DataObservable;
import PMSModel.Observers.IUserObserver;
import java.io.Serializable;
import java.util.ArrayList;

import utilities.Notification;

/**
 *
 * @author Goel
 */
public abstract class User extends DataObservable implements IUserObserver, Serializable {
    
    /**
     *
     */
    protected String ID;

    /**
     *
     */
    protected String password;

    /**
     *
     */
    protected String firstName;

    /**
     *
     */
    protected String lastName;

    /**
     *
     */
    protected Address address;

    /**
     *
     */
    protected UserType userType;
    
    //

    /**
     *
     */
    protected ArrayList<Notification> notifications;
    
    /**
     * 
     */
    public User() {
        
        this.ID = "UNKNOWN";
        this.password = null;
        this.firstName = "UNKNOWN";
        this.lastName = "UNKNOWN";
        this.address = null;
        this.userType = null;
        this.notifications = null;
        
        this.notifyObservers();
    }
    
    /**
     * 
     * @param ID
     * @param password
     * @param firstName
     * @param lastName
     * @param address 
     */
    public User(String ID, String password, String firstName, 
            String lastName, Address address) {
        
        this.ID = ID;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.userType = null;
        this.notifications = new ArrayList<Notification>();
        
        this.notifyObservers();
    }

    /**
     * 
     * @return 
     */
    public String getID() {
        return ID;
    }

    
    /**
     * 
     * @return 
     */
    public String getPassword() {
        return password;
    }

    
    /**
     * 
     * @param password 
     */
    public void setPassword(String password) {
        if (password != null && !password.isEmpty()) {
            this.password = password;
            
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    public String getFirstName() {
        return firstName;
    }

    
    /**
     * 
     * @param firstName 
     */
    public void setFirstName(String firstName) {
        if (firstName != null & !firstName.isEmpty()) {
            this.firstName = firstName;
            
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    public String getLastName() {
        return lastName;
    }

    
    /**
     * 
     * @param lastName 
     */
    public void setLastName(String lastName) {
        if (lastName != null & !lastName.isEmpty()) {
            this.lastName = lastName;
            
            this.notifyObservers();
        }
    }

    
    /**
     * 
     * @return 
     */
    public Address getAddress() {
        return address;
    }

    
    /**
     * 
     * @param address 
     */
    public void setAddress(Address address) {
        if (address != null) {
            this.address = address;
            
            this.notifyObservers();
        }
    }
    
    
    //

    /**
     *
     * @return
     */
    public abstract UserType getUserType();
    
    
    /**
    * 
    * @return 
    */
    public ArrayList<Notification> getNotifications() {
        return notifications;
    }

    
    /**
     * Implements the update method as a IUserObserver.
     * Creates a Notification from the messages that is sent from the observable.
     * @param newMessage 
     */
    @Override
    public void update(String newMessage) {
        if (!newMessage.isEmpty()) {
            Notification newNotification = new Notification(newMessage);
            this.notifications.add(newNotification);
            
            this.notifyObservers();
        }
    }
}
