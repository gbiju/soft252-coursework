/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package users;

import java.util.ArrayList;
import utilities.AdminFeedback;
import utilities.PatientFeedback;

/**
 *
 * @author Goel
 */
public class Doctor extends User {
    
    private float averageRating;
    
    // List of all the patient feedback that the Doctor has received as a PatientFeedback object.
    private ArrayList<PatientFeedback> patientFeedbacks;
    
    
    // List of all the administrator feedback that the Doctor has received as a AdminFeedback object.
    private ArrayList<AdminFeedback> adminFeedbacks;
    
    
    /**
     * 
     */
    public Doctor() {
        super();
    }
    
    
    /**
     * 
     * @param ID
     * @param password
     * @param firstName
     * @param lastName
     * @param address 
     */
    public Doctor(String ID, String password, String firstName, String lastName,
            Address address) {
        super(ID, password, firstName, lastName, address);
        
        this.patientFeedbacks = new ArrayList<PatientFeedback>();
        this.adminFeedbacks = new ArrayList<AdminFeedback>();
        
        this.notifyObservers();
    }
    
    
    /**
     * 
     * @return 
     */
    @Override
    public UserType getUserType() {
        return UserType.DOCTOR;
    }

    
    /**
     * 
     * @return 
     */
    public float getAverageRating() {
        return averageRating;
    }

    
    /**
     * Calculates a new average rating for the Doctor based on a new 
     * user rating given from a PatientFeedback object.
     * @param newUserRating 
     */
    private void calculateAverageRating(Integer newUserRating) {
        this.averageRating = (averageRating + newUserRating) / 2;
        
        this.notifyObservers();
    }

    
    /**
     * 
     * @return 
     */
    public ArrayList<PatientFeedback> getPatientFeedbacks() {
        return patientFeedbacks;
    }

    
    /**
     * 
     * @param newPatientFeedback 
     */
    public void addPatientFeedback(PatientFeedback newPatientFeedback) {
        //System.out.println("Adding patient feedback " + newPatientFeedback.getComments());
        
        // Calculate the Doctor's new average rating.
        this.calculateAverageRating(newPatientFeedback.getUserRating());
        
        // Add patient feedback into the patienFeedback list.
        this.patientFeedbacks.add(newPatientFeedback);
        
        System.out.println(this.patientFeedbacks);
        
        this.notifyObservers();
    }
    
    
    /***
     * 
     * @return 
     */
    public ArrayList<AdminFeedback> getAdminFeedbacks() {
        return adminFeedbacks;
    }
    
    
    /**
     * 
     * @param newAdminFeedback 
     */
    public void addAdminFeedback(AdminFeedback newAdminFeedback) {
        
        this.adminFeedbacks.add(newAdminFeedback);
        
        this.notifyObservers();
    }
    
}
