/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package users;

import java.io.Serializable;

/**
 *
 * @author Goel
 */
public class Address implements Serializable {
    
    private String addressLine;
    private String city;
    private String postCode;
    
    
    /**
     * 
     * @param addressLine
     * @param city
     * @param postCode 
     */
    public Address(String addressLine, String city, String postCode) {
        this.addressLine = addressLine;
        this.city = city;
        this.postCode = postCode;
    }

    
    /**
     * 
     * @return 
     */
    public String getAddressLine() {
        return addressLine;
    }

    
    /**
     * 
     * @param addressLine 
     */
    public void setAddressLine(String addressLine) {
        if (addressLine != null && !addressLine.isEmpty()) {
            this.addressLine = addressLine;
        }
    }

    
    /**
     * 
     * @return 
     */
    public String getCity() {
        return city;
    }

    
    /**
     * 
     * @param city 
     */
    public void setCity(String city) {
        if (city != null && !city.isEmpty()) {
            this.city = city;
        }
    }

    
    /**
     * 
     * @return 
     */
    public String getPostCode() {
        return postCode;
    }

    
    /**
     * 
     * @param postCode 
     */
    public void setPostCode(String postCode) {
        if (postCode != null && !postCode.isEmpty()) {
            this.postCode = postCode;
        }
    }
    
    
    /**
     * 
     * @return 
     */
    public String getFullAddress() {
        return this.addressLine + ", " + this.city + ", " + this.postCode;
    }
}
