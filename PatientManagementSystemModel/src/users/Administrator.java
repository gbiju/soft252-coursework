/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package users;


/**
 *
 * @author Goel
 */
public class Administrator extends User {
    
    
    /**
     * 
     */
    public Administrator() {
        super();
    }
    
    
    /**
     * 
     * @param ID
     * @param password
     * @param firstName
     * @param lastName
     * @param address 
     */
    public Administrator(String ID, String password, String firstName, 
            String lastName, Address address) {
        super(ID, password, firstName, lastName, address);
    }
    
    
    /**
     * 
     * @return 
     */
    @Override
    public UserType getUserType() {
        return UserType.ADMINISTRATOR;
    }
}
