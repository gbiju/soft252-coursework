/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import users.Patient;

/**
 *
 * @author Goel
 */
public class PatientFeedback extends Feedback implements Comparable<PatientFeedback> {
    
    private Integer userRating;
    
    /**
     * 
     * @param patient
     * @param dateTime
     * @param patientComments 
     * @param userRating 
     */
    public PatientFeedback(Patient patient, LocalDateTime dateTime, String patientComments, Integer userRating) {
        super(patient, dateTime, patientComments);
        
        this.userRating = userRating;
    }

    
    /**
     * 
     * @return 
     */
    public Integer getUserRating() {
        return userRating;
    }

    
    /**
     * 
     * @param userRating 
     */
    public void setUserRating(Integer userRating) {
        if (userRating > 0) {
            this.userRating = userRating;
        }
    }
    
    
    /**
     * 
     * @param t
     * @return 
     */
    @Override
    public int compareTo(PatientFeedback t) {
        int result = 0;
        LocalDateTime tTime = t.getDateTime();
        if (!dateTime.equals(tTime)) {
            if (dateTime.isBefore(tTime)) {
                result = -1;
            } else {
                result = 1;
            }
        }
        return result;
    }
    
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof PatientFeedback) {
            PatientFeedback target = (PatientFeedback)o;
            if (0 == this.compareTo(target)) {
                if (this.user.getID().equals(target.getUser().getID())) {
                    if (this.comments.equals(target.getComments())) {
                        if (this.userRating.equals(target.getUserRating())) {
                            result = true;
                        }
                    }
                }
            }
        }
        return result;
    }

    
    /**
     * 
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.userRating);
        return hash;
    }
    
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        result.append(this.dateTime.format(format));
        
        // Create a string which describes the patient feedback object.
        if (this.userRating > 0 && !this.comments.isEmpty()) {
            result.append(" - Patient ");
            result.append(this.user.getID());
            result.append(" provided feedback with a rating: ");
            result.append(this.userRating);
            result.append("/10");
        }
        
        return result.toString();
    }
}
