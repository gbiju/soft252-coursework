/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 *
 * @author Goel
 */
public class Notification implements Comparable<Notification>, Serializable {
    
    private LocalDateTime dateTime;
    private String message;
    
    
    /**
     * 
     * @param newMessage 
     */
    public Notification(String newMessage) {
        this.dateTime = LocalDateTime.now();
        this.message = newMessage;
    }

    
    /**
     * 
     * @return 
     */
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    
    /**
     * 
     * @param dateTime 
     */
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
    
    
    /**
     * 
     * @return 
     */
    public String getMessage() {
        return message;
    }

    
    /**
     * 
     * @param message 
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    
    /**
     * 
     * @param t
     * @return 
     */
    @Override
    public int compareTo(Notification t) {
        int result = 0;
        LocalDateTime tTime = t.getDateTime();
        if (!dateTime.equals(tTime)) {
            if (dateTime.isAfter(tTime)) {
                result = -1;
            } else {
                result = 1;
            }
        }
        return result;
    }
    
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof Notification) {
            Notification target = (Notification)o;
            if (0 == this.compareTo(target)) {
                if (this.message.equals(target.getMessage())) {
                    result = true;
                }
            }
        }
        return result;
    }

    
    /**
     * 
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.dateTime);
        hash = 97 * hash + Objects.hashCode(this.message);
        return hash;
    }
    
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        result.append(this.dateTime.format(format));
        
        // Append the notification message along with the date when 
        // it was received.
        if (!this.message.isEmpty()) {
            result.append(" - ");
            result.append(this.message);
        }
        
        return result.toString();
    }
}
