/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;
import users.User;

/**
 *
 * @author Goel
 */
public class Feedback implements Serializable {
    
    /**
     *
     */
    protected User user;

    /**
     *
     */
    protected String comments;

    /**
     *
     */
    protected LocalDateTime dateTime;
    
    
    /**
     * 
     * @param user
     * @param dateTime
     * @param userComments 
     */
    public Feedback(User user, LocalDateTime dateTime, String userComments) {
        this.user = user;
        this.comments = userComments;
        this.dateTime = dateTime;
    }

    
    /**
     * 
     * @return 
     */
    public User getUser() {
        return user;
    }

    
    /**
     * 
     * @param user 
     */
    public void setUser(User user) {
        this.user = user;
    }

    
    /**
     * 
     * @return 
     */
    public String getComments() {
        return comments;
    }

    
    /**
     * 
     * @param newComments 
     */
    public void setComments(String newComments) {
        if (!newComments.isEmpty()) {
            this.comments = newComments;
        }
    }

    
    /**
     * 
     * @return 
     */
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    
    /**
     * 
     * @param dateTime 
     */
    public void setDateTime(LocalDateTime dateTime) {
        if (dateTime != null) {
            this.dateTime = dateTime;
        }
    }    
}
