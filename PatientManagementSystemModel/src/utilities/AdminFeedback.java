/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import users.Administrator;

/**
 *
 * @author Goel
 */
public class AdminFeedback extends Feedback implements Comparable<AdminFeedback> {
    
    /**
     *
     * @param administrator
     * @param dateTime
     * @param adminComments
     */
    public AdminFeedback(Administrator administrator, LocalDateTime dateTime, 
            String adminComments) {
        super(administrator, dateTime, adminComments);
    }
    
    
    /**
     * 
     * @param t
     * @return 
     */
    @Override
    public int compareTo(AdminFeedback t) {
        int result = 0;
        LocalDateTime tTime = t.getDateTime();
        if (!dateTime.equals(tTime)) {
            if (dateTime.isBefore(tTime)) {
                result = -1;
            } else {
                result = 1;
            }
        }
        return result;
    }
    
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof AdminFeedback) {
            AdminFeedback target = (AdminFeedback)o;
            if (0 == this.compareTo(target)) {
                if (this.user.getID().equals(target.getUser().getID())) {
                    if (this.comments.equals(target.getComments())) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }
    

    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        result.append(this.dateTime.format(format));
        
        // Create the final feedback message as one string.
        if (this.user != null && !this.comments.isEmpty()) {
            result.append(" - Feedback from Admin. ");
            result.append(this.user.getFirstName());
            result.append(" ");
            result.append(this.user.getLastName());
        }
        
        return result.toString();
    }

    
    /**
     * 
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }
}
