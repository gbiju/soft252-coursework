/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Goel
 */
public class NotificationList implements Iterable<Notification>, Serializable {
    
    /**
     *
     */
    public final List<Notification> data;
    
    /**
     *
     * @param notifications
     */
    public NotificationList(ArrayList<Notification> notifications) {
        this.data = notifications;
    }
    
    @Override
    public Iterator<Notification> iterator() {
        return data.iterator();
    }
}
