/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 * Interface for classes that will make use of serialisable behaviours.
 * @author Goel
 */
public interface ISerialisable {
    
    /**
     * 
     */
    public void readData();
    
    /**
     * 
     */
    public void saveData();
}
