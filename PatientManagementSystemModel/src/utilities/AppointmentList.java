/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import appointments.Appointment;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Goel
 */
public class AppointmentList implements Iterable<Appointment>, Serializable {
    
    /**
     *
     */
    public final List<Appointment> data;
    
    /**
     *
     */
    public AppointmentList() {
        this.data = new ArrayList<>();
    }
    
    /**
     *
     * @param appointments
     */
    public AppointmentList(ArrayList<Appointment> appointments) {
        this.data = appointments;
    }
    
    /**
     *
     * @param newAppointment
     * @return
     */
    public boolean addAppointment(Appointment newAppointment) {
        boolean result = false;
        if (null != newAppointment) {
            result = data.add(newAppointment);
            Collections.sort(data);
        }
        return result;
    }
    
    @Override
    public Iterator<Appointment> iterator() {
        return data.iterator();
    }
    
}
