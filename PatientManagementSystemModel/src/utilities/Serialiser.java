/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.*;

/**
 *
 * @author Goel
 */
public class Serialiser {
    
    private String fileName;
    
    /**
     *
     * @param fileName
     */
    public Serialiser(String fileName) {
        this.fileName = fileName;
        
        // Create directory.
        File dir = new File("data");
        if (!dir.exists()) {
            if(dir.mkdir()) {
                System.out.println("Created directory for data to reside in.");
            };
        }
    }

    /**
     *
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     *
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    /**
     *
     * @return
     */
    public Serializable readObject() {
        Serializable loadedObject = null;
        File objFile = new File(this.fileName);
        
        if (objFile.exists() && objFile.canRead()) {
            try {
                FileInputStream fileIn = new FileInputStream(this.fileName);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                loadedObject = (Serializable) in.readObject();
                in.close();
                fileIn.close();
                System.out.println("Serialised data loaded from: " + this.fileName);
            } catch (IOException i) {
                System.out.println("File was not found.");
                i.printStackTrace();
            } catch (ClassNotFoundException c) {
                System.out.println("Class was not found.");
                c.printStackTrace();
            }
        } else {
            System.out.println("File does not exist.");
        }
        
        return loadedObject;
    }
    
    /**
     *
     * @param object
     * @return
     */
    public boolean writeObject(Serializable object) {
        File objFile = new File(this.fileName);
        try {
            objFile.createNewFile();
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage().toString());
        }
        
        try {
            FileOutputStream fileOut = new FileOutputStream(this.fileName);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(object);
            out.close();
            fileOut.close();
            System.out.println("Serialised data is now saved in: " + this.fileName);
            return true;
        } catch (IOException i) {
            System.out.println("Failed to load data.");
            i.printStackTrace();
            return false;
        }
    }
}
