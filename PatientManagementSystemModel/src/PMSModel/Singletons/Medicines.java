/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Singletons;

import utilities.ISerialisable;
import PMSModel.Observables.DataObservable;
import appointments.Medicine;
import appointments.Prescription;
import java.util.ArrayList;
import users.Doctor;
import users.Patient;
import users.Secretary;
import PMSModel.Observers.MedicinesChangeObserver;
import utilities.Serialiser;

/**
 *
 * @author Goel
 */
public class Medicines extends DataObservable implements ISerialisable {
    
    private static Medicines uniqueInstance = null;
    private static ArrayList<Medicine> medicines;
    
    //private ArrayList<IUserObserver> observers;
    
    private Serialiser medicineSerialiser;
    
    
    private Medicines() {
        
        medicines = new ArrayList<Medicine>();
        medicineSerialiser = new Serialiser("data//medicines.ser");
    }
    
    
    /**
     * 
     * @return 
     */
    public static Medicines getInstance() {
        // If there is no instance already, create one.
        if (uniqueInstance == null) {
            uniqueInstance = new Medicines();
        }
        
        return uniqueInstance;
    }
    
    
    /**
     * 
     */
    @Override
    public void readData() {
        System.out.println("Reading data in from Medicines.");
        
        // Load the medicines file from disk.
        ArrayList<Medicine> retrievedMedicines = (ArrayList) medicineSerialiser.readObject();
        System.out.println("Retrieved Medicines: " + retrievedMedicines);
        
        if (retrievedMedicines != null) {
            medicines = retrievedMedicines;
            
            // Medicine instance.
            Medicines medicinesInstance = Medicines.getInstance();
            
            // Register all secretaries as observers of this administrators observable.
            Secretaries secretariesInstance = Secretaries.getInstance();
            ArrayList<Secretary> allSecretaries = secretariesInstance.getSecretaries();
            
            for (Secretary secretary: allSecretaries) {
                medicinesInstance.registerUserObserver(secretary);
            }
            
            // Register adminstrator changed observers.
            MedicinesChangeObserver changeObserver = new MedicinesChangeObserver();
            this.registerObserver(changeObserver);
            
            System.out.println("Read in data for Medicines.");
        } else {
            System.out.println("No initial file, creating new file with Medicines.");
            // If the file does not exist already, create one with a blank array list of administrators.
            saveData();
            
            // Call read data again.
            readData();
        }
    }
    
    
     /**
     * 
     */
    @Override
    public void saveData() {
        // Write the patients list into the 
        medicineSerialiser.writeObject(medicines);
    }
    
    
    
    /**
     * 
     * @param patient
     * @param prescriptions 
     * @return  
     */
    public boolean collectMedicine(Patient patient, ArrayList<Prescription> prescriptions) {
        boolean collectedPrescriptions = false;
        if (medicines.size() > 0) {
            if (patient != null && prescriptions != null && prescriptions.size() > 0) {
                // Run through the prescriptions and subtract stock of medicine
                // relative to the quantity specified in the prescriptions.
                for (Prescription prescription: prescriptions) {
                    Medicine prescriptionMedicine = prescription.getMedicine();

                    Integer currentQty = prescriptionMedicine.getQuantityInStock();

                    // Emsure there is enough to supply the patient with.
                    if (currentQty >= prescription.getPrescriptionQuantity()) {
                        prescriptionMedicine.setQuantityInStock(currentQty - prescription.getPrescriptionQuantity());
                        
                        // Set the prescription to collected.
                        prescription.setCollectedPrescription(true);
                    } else {
                        // Notify the patient that this specific medicine is not available right now.
                        patient.update("Medicine Collection - Please come back later to collect " + 
                                prescriptionMedicine.getName() + " (" + prescription.getPrescriptionQuantity() + ").");
                    }
                }
                
                // Notify all observers of this collection.
                this.notifyUserObservers("Medicine Collection - Patient ID (" + patient.getID() + ") collected their prescription.");
                this.notifyObservers();
                
                collectedPrescriptions = true;
            }
        } else {
            // Directly send a message to the patient.
            patient.update("Medicine Collection - Please come back to collect all of your stock, we are fully out of stock.");
        }
        return collectedPrescriptions;
    }
    
    
    /**
     * 
     * @param secretary
     * @param medicine
     * @return 
     */
    public boolean replenishStock(Secretary secretary, Medicine medicine) {
        boolean replenishedStock = false;
        if (medicines.size() > 0) {
            if (medicines.contains(medicine)) {
                Integer idx = medicines.indexOf(medicine);
                
                // Replenish the stock by placing and   order for the normal quantity 
                // in which the medicine is bought in.
                medicine.setQuantityInStock(medicine.getQuantityInStock() + medicine.getQuantityToBuy());
                
                this.notifyUserObservers("Medicine - Stock for " + medicine.getName() + 
                        " has been ordered by Secretary " + secretary.getFirstName() + " " +
                        secretary.getLastName() + " (" + secretary.getID() + ")");
                this.notifyObservers();
                replenishedStock = true;
            }
        }
        return replenishedStock;
    }
    
    
    /**
     * 
     * @param doctor
     * @param medicine
     * @return 
     */
    public boolean createMedicine(Doctor doctor, Medicine medicine) {
        boolean createdMedicine = false;
        
        if (doctor != null && medicine != null && !medicine.getName().isEmpty()) {
            medicines.add(medicine);
            
            this.registerUserObserver(doctor);
            
            // Notify all observers of the new medicines (secretaries will have received this.
            this.notifyUserObservers("Medicine - New Medicine Created by Dr. " + 
                    doctor.getLastName() + " (" + doctor.getID() + "): " + 
                    medicine.getName() + ", secretaries please order.");
            this.notifyObservers();
            createdMedicine = true;
        }
        
        return createdMedicine;
    }
    
    
    /**
     *
     * @return
     */
    public ArrayList<Medicine> getMedicines() {
        if (medicines == null) {
            return new ArrayList<Medicine>();
        }
        
        ArrayList<Medicine> temp = new ArrayList<Medicine>();
        
        for (Medicine medicine: medicines) {
            temp.add(medicine);
        }
        return temp;
    }
}
