/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Singletons;

import utilities.ISerialisable;
import PMSModel.Observables.DataObservable;
import java.util.ArrayList;
import java.util.Random;
import users.Administrator;
import users.Secretary;
import PMSModel.Observers.AdministratorsChangeObserver;
import utilities.Serialiser;

/**
 *
 * @author Goel
 */
public class Administrators extends DataObservable implements ISerialisable {
    
    private static Administrators uniqueInstance = null;
    private static ArrayList<Administrator> administrators;
    
    private Serialiser administratorSerialiser;
    
    
    private Administrators() {
        
        administrators = new ArrayList<Administrator>();
        administratorSerialiser = new Serialiser("data//administrators.ser");
    }
    
    /**
     *
     * @return
     */
    public static Administrators getInstance() {
        
        if (uniqueInstance == null) {
            uniqueInstance = new Administrators();
        }
        
        return uniqueInstance;
    }
    
    
    /**
     * 
     */
    @Override
    public void readData() {
        System.out.println("Reading data in from Administrators.");

        // Load the admins file from disk.
        ArrayList<Administrator> retrievedAdmins = (ArrayList) administratorSerialiser.readObject();
        System.out.println("Retrieved Admins: " + retrievedAdmins);
        
        if (retrievedAdmins != null) {
            administrators = retrievedAdmins;
            
            // Get instance of administrators.
            Administrators administratorsInstance = Administrators.getInstance();
            
            // Register all secretaries as observers of this administrators observable.
            Secretaries secretaries = Secretaries.getInstance();
            ArrayList<Secretary> allSecretaries = secretaries.getSecretaries();
            
            for (Secretary secretary: allSecretaries) {
                administratorsInstance.registerUserObserver(secretary);
            }
            
            // Register all administrators as observers of this administrators observable.
            for (Administrator admin: administrators) {
                this.registerUserObserver(admin);
            }
            
            // Register adminstrator changed observers.
            AdministratorsChangeObserver changeObserver = new AdministratorsChangeObserver();
            
            for (Administrator admin: administrators) {
                admin.registerObserver(changeObserver);
            } 
            this.registerObserver(changeObserver);
            
            System.out.println("Read in data for Administrators.");
            
        } else {
            System.out.println("No initial file, creating new file with Administrators.");
            // If the file does not exist already, create one with a blank array list of administrators.
            saveData();
            
            // Call read data again.
            readData();
        }
    }
    
    /**
     * 
     */
    @Override
    public void saveData() {
        // Write the administrators list into the 
        administratorSerialiser.writeObject(administrators);
        System.out.println("Saved data for Administrators.");
    }
    
    
    /**
     * 
     * @param userLetter
     * @return 
     */
    public String getNextID(String userLetter) {
        // Create a 4 digit integer with leading zeroes in a string.
        String id = userLetter + String.format("%04d", administrators.size() + 1);
        return id;
    }
    
    
    /**
     * 
     * @param admin
     * @return 
     */
    public Boolean createAdministratorAccount(Administrator admin) {
        boolean addedAdmin = false;
        if (admin != null) {
            addedAdmin = administrators.add(admin);
            
            // Register this admin object.
            this.registerUserObserver(admin);
            
            // Notify secretaries and administrators of the new admin account.
            this.notifyUserObservers("Administrator Account Created - ID: " + admin.getID() + 
                    " Name: " + admin.getFirstName() + " " + admin.getLastName());
            this.notifyObservers();
        }
        return addedAdmin;
    }
    
    
    /**
     * 
     * @return 
     */
    public ArrayList<Administrator> getAdministrators() {
        if (administrators == null) {
            return new ArrayList<Administrator>();
        }
        
        ArrayList<Administrator> temp = new ArrayList<Administrator>();
        
        for (Administrator admin : administrators) {
            temp.add(admin);
        }
        return temp;
    }
}
