/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Singletons;

import utilities.ISerialisable;
import PMSModel.Observables.DataObservable;
import appointments.Appointment;
import java.time.LocalDateTime;
import java.util.ArrayList;
import users.Doctor;
import users.Patient;
import users.Secretary;
import PMSModel.Observers.AppointmentsChangeObserver;
import utilities.Serialiser;

/**
 *
 * @author Goel
 */
public class Appointments extends DataObservable implements ISerialisable {
    
    private static Appointments uniqueInstance = null;
    private static ArrayList<Appointment> appointments;
    
    private Serialiser appointmentSerialiser;
    
    
    private Appointments() {
        
        appointments = new ArrayList<Appointment>();
        appointmentSerialiser = new Serialiser("data//appointments.ser");
    }
    
    
    /**
     * 
     * @return 
     */
    public static Appointments getInstance() {
        // If there is no instance already, create one.
        if (uniqueInstance == null) {
            uniqueInstance = new Appointments();
        }
        
        return uniqueInstance;
    }
    
    
    /**
     * 
     */
    @Override
    public void readData() {
        System.out.println("Reading data in from Appointments.");
        
        // Load the appointments file from disk.
        ArrayList<Appointment> retrievedAppointments = (ArrayList) appointmentSerialiser.readObject();
        System.out.println("Retrieved Appointments: " + retrievedAppointments);
        
        if (retrievedAppointments != null) {
            appointments = retrievedAppointments;
            
            // Appointments instance.
            Appointments appointmentsInstance = Appointments.getInstance();
            
            // Register all secretaries as observers of this appointments observable.
            Secretaries secretaries = Secretaries.getInstance();
            ArrayList<Secretary> allSecretaries = secretaries.getSecretaries();
            
            for (Secretary secretary: allSecretaries) {
                appointmentsInstance.registerUserObserver(secretary);
            }
            
            // Register appointment changed observers.
            AppointmentsChangeObserver changeObserver = new AppointmentsChangeObserver();
            this.registerObserver(changeObserver);
            
            System.out.println("Read in data for Appointments.");
        } else {
            System.out.println("No initial file, creating new file with Appointments.");
            // If the file does not exist already, create one with a blank array list of administrators.
            saveData();
            
            // Call read data again.
            readData();
        }
    }
    
    
     /**
     * 
     */
    @Override
    public void saveData() {
        // Write the appointments list.
        System.out.println("Saving the Appointments file.");
        appointmentSerialiser.writeObject(appointments);
    }
    
    
    /**
     * 
     * @param patient
     * @param doctor
     * @param requestedDateTimes
     * @return 
     */
    public boolean requestAppointment(Patient patient, Doctor  doctor, ArrayList<LocalDateTime> requestedDateTimes) {
        boolean requestedAppointment = false;
        if (patient != null && doctor != null && requestedDateTimes != null) {
            System.out.println("Creating new appointment for request.");
            Appointment newAppointment = new Appointment(patient, doctor, requestedDateTimes);

            // Associate patient as observer of appointment now.
            newAppointment.registerUserObserver(patient);
            
            // Add the appointment into array.
            requestedAppointment = appointments.add(newAppointment);
            
            // Notify secretaries.
            this.notifyUserObservers("Patient Appointment Request: " + patient.getID() + " (" + patient.getFirstName() + " " + patient.getLastName() + ")");
            this.notifyObservers();
        }
        
        return requestedAppointment;
    }
    
    
    /**
     * 
     * @param secretary
     * @param appointment
     * @return 
     */
    public boolean removeAppointment(Secretary secretary, Appointment appointment) {
       boolean success = false;
       if (appointment != null) {
           success = appointments.remove(appointment);
           
           appointment.notifyUserObservers("Requested Appointment Removed: Patient (" + appointment.getPatient().getID() + ") by Secretary " +
                   secretary.getFirstName() + " " + secretary.getLastName());
           this.notifyObservers();
       }
       return success;
    }
    
    
    /**
     * 
     * @param prevRequestedAppointment
     * @param confirmedDateTime
     * @return 
     */
    public boolean confirmAppointment(Appointment prevRequestedAppointment, LocalDateTime confirmedDateTime) {
        boolean confirmedAppointment = false;
        if (prevRequestedAppointment != null && confirmedDateTime != null) {
            if (appointments.contains(prevRequestedAppointment)) {
                // Get the integer.
                Integer idx = appointments.indexOf(prevRequestedAppointment);
                
                if (idx > -1) {
                    // Set the date time that has been confirmed for the appointment.
                    prevRequestedAppointment.setConfirmedDateTime(confirmedDateTime);
                    prevRequestedAppointment.setConfirmedAppointment(confirmedAppointment);
                    
                    // Notify all those who are related with the appointment.
                    // We can call notify on the Appointment, observable object.
                    prevRequestedAppointment.registerUserObserver(prevRequestedAppointment.getDoctor());

                    prevRequestedAppointment.notifyUserObservers("Appointment Confirmed: Patient (" + 
                            prevRequestedAppointment.getPatient().getID() + ") with Dr. " + 
                            prevRequestedAppointment.getDoctor().getFirstName() + " " + prevRequestedAppointment.getDoctor().getLastName());
                    
                    // Set the appointment back into the list.
                    appointments.set(idx, prevRequestedAppointment);
                    
                    this.notifyObservers();
                    
                    confirmedAppointment = true;
                }
            }
        }
        return confirmedAppointment;
    }
    
    
    /**
     * 
     * @param followupAppointment
     * @param confirmedDateTime
     * @return 
     */
    public boolean bookFollowUpAppointment(Appointment followupAppointment, LocalDateTime confirmedDateTime) {
        boolean confirmedAppointment = false;
        if (followupAppointment != null && confirmedDateTime != null) {
            // Notify all those who are related with the appointment.
            // We can call notify on the Appointment, observable object.
            followupAppointment.registerUserObserver(followupAppointment.getPatient());
            followupAppointment.registerUserObserver(followupAppointment.getDoctor());

            followupAppointment.notifyUserObservers("Follow-up Appointment Confirmed: Patient (" + 
                    followupAppointment.getPatient().getID() + ") with Dr. " + 
                    followupAppointment.getDoctor().getFirstName() + " " + followupAppointment.getDoctor().getLastName());

            // Set the appointment back into the list.
            appointments.add(followupAppointment);

            this.notifyObservers();

            confirmedAppointment = true;
        }
        return confirmedAppointment;
    }
    
    
    /**
     * 
     * @return 
     */
    public ArrayList<Appointment> getAppointments() {
        if (appointments == null) {
            return new ArrayList<Appointment>();
        }

        ArrayList<Appointment> temp = new ArrayList<Appointment>();

        for (Appointment appointment : appointments) {
            temp.add(appointment);
        }
        return temp;
    }
    
    
    
    /**
     * 
     * @param patient
     * @return 
     */
    public ArrayList<Appointment> getPatientAppointments(Patient patient) {
        if (patient != null) {
            if (appointments == null) {
                return new ArrayList<Appointment>();
            }
            
            ArrayList<Appointment> temp = new ArrayList<Appointment>();
            
            for (Appointment appointment : appointments) {
                if (appointment.getPatient().getID().equals(patient.getID())) {
                    temp.add(appointment);
                }
            }
            return temp;
        } else {
            return null;
        }
    }
    
    
    /**
     * 
     * @param doctor
     * @return 
     */
    public ArrayList<Appointment> getDoctorAppointment(Doctor doctor) {
        if (doctor != null) {
            if (appointments == null) {
                return new ArrayList<Appointment>();
            }
            
            ArrayList<Appointment> temp = new ArrayList<Appointment>();
            
            for (Appointment appointment : appointments) {
                if (appointment.getDoctor().getID().equals(doctor.getID())) {
                    temp.add(appointment);
                }
            }
            return temp;
        } else {
            return null;
        }
    }
}
