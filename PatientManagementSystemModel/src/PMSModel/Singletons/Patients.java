/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Singletons;

import utilities.ISerialisable;
import PMSModel.Observables.DataObservable;
import java.util.ArrayList;
import java.util.Random;
import users.Patient;
import users.Secretary;
import PMSModel.Observers.PatientsChangeObserver;
import utilities.Serialiser;

/**
 *
 * @author Goel
 */
public class Patients extends DataObservable implements ISerialisable {
    
    private static Patients uniqueInstance = null;
    private static ArrayList<Patient> patients;
    
    private Serialiser patientSerialiser;
    
    /**
     * 
     */
    private Patients() {
        
        // Intialise the patients list and the patient serialiser.
        patients = new ArrayList<Patient>();
        patientSerialiser = new Serialiser("data//patients.ser");
    }
    
    
     /**
     * 
     * @return 
     */
    public static Patients getInstance() {
        // If there is no instance already, create one.
        if (uniqueInstance == null) {
            uniqueInstance = new Patients();
        }
        
        return uniqueInstance;
    }
    
    
    /**
     * 
     */
    @Override
    public void readData() {
        System.out.println("Reading data in from Patients.");
        
        // Load the patients file from disk.
        ArrayList<Patient> retrievedPatients = (ArrayList) patientSerialiser.readObject();
        System.out.println("Retrieved Patients: " + retrievedPatients);
        
        if (retrievedPatients != null) {
            patients = retrievedPatients;
            
            // Get patients instance.
            Patients patientsInstance = Patients.getInstance();
            
            // Register all secretaries as observers of this patients observable.
            Secretaries secretaries = Secretaries.getInstance();
            ArrayList<Secretary> allSecretaries = secretaries.getSecretaries();
            
            for (Secretary secretary: allSecretaries) {
                patientsInstance.registerUserObserver(secretary);
            }
            
            // Register patient changed observers.
            PatientsChangeObserver changeObserver = new PatientsChangeObserver();
            for (Patient patient: patients) {
                patient.registerObserver(changeObserver);
            }
            
            this.registerObserver(changeObserver);
            
            System.out.println("Read in data for Patients.");
        } else {
            System.out.println("No initial file, creating new file with Patients.");
            // If the file does not exist already, create one with a blank array list of administrators.
            saveData();
            
            // Call read data again.
            readData();
        }
    }
    
    /**
     * 
     */
    @Override
    public void saveData() {
        // Write the patients list into the 
        patientSerialiser.writeObject(patients);
        System.out.println("Saved data for Patients.");
    }
    
    /**
     * 
     * @param userLetter
     * @return 
     */
    public String getNextID(String userLetter) {
        // Create a 4 digit integer with leading zeroes in a string.
        String id = userLetter + String.format("%04d", patients.size() + 1);
        return id;
    }
    
    /**
     * 
     * @param newPatient 
     * @return  
     */
    public boolean requestPatientAccount(Patient newPatient) {
        boolean success = false;
        if (newPatient != null && (newPatient.getAccountApproved() == false)) {
            success = patients.add(newPatient);
            
            // Notify ALL SECRETARIES about patient account request.
            this.notifyUserObservers("Patient Account Requested: " + newPatient.getID() + 
                    " (" + newPatient.getFirstName() + " " + newPatient.getLastName() + ")");
            this.notifyObservers();
        }
        return success;
    }
    
    
    /**
     * 
     * @param patient 
     * @return  
     */
    public boolean approvePatientAccount(Patient patient) {
        boolean success = false;
        if (patient != null && (patient.getAccountApproved() != true)) {
            patient.setAccountApproved(true);
            
            // Notify patient that account is approved?
            this.notifyUserObservers("Patient Account Approved: " + patient.getID() + 
                    " (" + patient.getFirstName() + " " + patient.getLastName() + ")");
            this.notifyObservers();
            success = true;
        }
        return success;
    }
    
    
   /**
    * 
    * @param patient
    * @return 
    */
   private boolean removePatientAccount(Patient patient) {
       boolean success = false;
       if (patient != null) {
           success = patients.remove(patient);
           this.notifyObservers();
       }
       return success;
   }
    
   
   /**
    * 
    * @param patient
    * @return 
    */
   public boolean requestPatientAccountTermination(Patient patient) {
       boolean success = false;
       if (patient != null) {
           patient.setRequestedTermination(true);
           
           // Notify all secretaries of this.
           this.notifyUserObservers("Patient Account Termination Requested: " + patient.getID() + 
                   " (" + patient.getFirstName() + " " + patient.getLastName() + ")");
           this.notifyObservers();
           success = true;
       }
       return success;
   }
   
   
   
  /**
   * 
   * @param patient
   * @return 
   */
   public boolean approvePatientAccountTermination(Patient patient) {
        boolean success = false;
        if (patient != null && (patient.getRequestedTermination() != false)) {
            // the request is now approved and the patient will be removed
            success = this.removePatientAccount(patient);
            
            // Notify patient that account is approved?
            this.notifyUserObservers("Patient Account Termination Approved: " + patient.getID() + 
                    " (" + patient.getFirstName() + " " + patient.getLastName() + ")");
            this.notifyObservers();
        }
        return success;
    }
   
    /**
     *
     * @return
     */
    public ArrayList<Patient> getPatients() {
       if (patients == null) {
           return new ArrayList<Patient>();
       }
       
       ArrayList<Patient> temp = new ArrayList<Patient>();
       
       for (Patient patient : patients) {
           temp.add(patient);
       }
       return temp;
   }
}
