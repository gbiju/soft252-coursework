/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Singletons;

import utilities.ISerialisable;
import PMSModel.Observables.DataObservable;
import java.util.ArrayList;
import java.util.Random;
import users.Administrator;
import users.Secretary;
import PMSModel.Observers.SecretariesChangeObserver;
import utilities.Serialiser;

/**
 *
 * @author Goel
 */
public class Secretaries extends DataObservable implements ISerialisable {
    
    private static Secretaries uniqueInstance = null;
    private static ArrayList<Secretary> secretaries;
    
    //private ArrayList<IUserObserver> observers;
    
    private Serialiser secretarySerialiser;
    
    /**
     * 
     */
    private Secretaries() {
        
        secretaries = new ArrayList<Secretary>();
        secretarySerialiser = new Serialiser("data//secretaries.ser");
    }
    
    
    
    /**
     * 
     * @return 
     */public static Secretaries getInstance() {
        // If there is no instance already, create one.
        if (uniqueInstance == null) {
            uniqueInstance = new Secretaries();
        }
        
        return uniqueInstance;
    }
    
    
    /**
     * 
     */
    @Override
    public void readData() {
        System.out.println("Reading data in from Secretaries.");
        
        // Load the secretaries file from disk.
        ArrayList<Secretary> retrievedSecretaries = (ArrayList) secretarySerialiser.readObject();
        System.out.println("Retrieved Secretaries: " + retrievedSecretaries);
        
        if (retrievedSecretaries != null) {
            secretaries = retrievedSecretaries;
            
            // Register all secretaries as observers of this secretaries observable.
            Secretaries secretariesInstance = Secretaries.getInstance();
            ArrayList<Secretary> allSecretaries = secretariesInstance.getSecretaries();
            
            for (Secretary secretary: allSecretaries) {
                secretariesInstance.registerUserObserver(secretary);
            }
            
            // Register all administrators as observers of this secretaries observable.
            Administrators administratorsInstance = Administrators.getInstance();
            ArrayList<Administrator> allAdmins = administratorsInstance.getAdministrators();
            
            for (Administrator administrator: allAdmins) {
                secretariesInstance.registerUserObserver(administrator);
            }
            
            // Register secretaries changed observers.
            SecretariesChangeObserver changeObserver = new SecretariesChangeObserver();
            this.registerObserver(changeObserver);
            
            System.out.println("Read in data for Secretaries.");
        } else {
            System.out.println("No initial file, creating new file with Secretaries.");
            // If the file does not exist already, create one with a blank array list of administrators.
            saveData();
            
            // Call read data again.
            readData();
        }
    }
    
    /**
     * 
     */
    @Override
    public void saveData() {
        // Write the secretaries list into the 
        secretarySerialiser.writeObject(secretaries);
    }
    
   /**
     * 
     * @param userLetter
     * @return 
     */
    public String getNextID(String userLetter) {
        // Create a 4 digit integer with leading zeroes in a string.
        String id = userLetter + String.format("%04d", secretaries.size() + 1);
        return id;
    }
    
    /**
     * 
     * @param admin
     * @param secretary
     * @return 
     */
    public Boolean createSecretaryAccount(Administrator admin, Secretary secretary) {
        boolean success = false;
        if (secretary != null) {
            success = secretaries.add(secretary);
            
            this.registerUserObserver(admin);
            
            this.notifyUserObservers("Secretary Account Added - New ID (" + secretary.getID() + ") by Administrator (" + admin.getID() + ").");
            this.notifyObservers();
        }
        
        return success;
    }
    
    
    /**
     * 
     * @param admin
     * @param secretary
     * @return 
     */
    public Boolean removeSecretaryAccount(Administrator admin, Secretary secretary) {
        boolean success = false;
        if (secretary != null && secretaries.size() > 0) {
            success = secretaries.remove(secretary);
            
            this.registerUserObserver(admin);
            
            this.notifyUserObservers("Secretary Account Removed - Remvoed ID (" + secretary.getID() + ") by Administrator (" + admin.getID() + ").");
            this.notifyObservers();
        }
        
        return success;
    }
    
    
    /**
     * 
     * @return 
     */
    public ArrayList<Secretary> getSecretaries() {
        if (secretaries == null) {
            return new ArrayList<Secretary>();
        }
        
        ArrayList<Secretary> temp = new ArrayList<Secretary>();
        
        for (Secretary secretary : secretaries) {
            temp.add(secretary);
        }
        return temp;
    }
}
