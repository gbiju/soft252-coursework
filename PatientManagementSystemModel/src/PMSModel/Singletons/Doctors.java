/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Singletons;

import utilities.ISerialisable;
import PMSModel.Observables.DataObservable;
import java.util.ArrayList;
import java.util.Random;
import users.Administrator;
import users.Doctor;
import users.Secretary;
import utilities.AdminFeedback;
import PMSModel.Observers.DoctorsChangeObserver;
import utilities.PatientFeedback;
import utilities.Serialiser;

/**
 *
 * @author Goel
 */
public class Doctors extends DataObservable implements ISerialisable {
    
    private static Doctors uniqueInstance = null;
    private static ArrayList<Doctor> doctors;
    
    private Serialiser doctorSerialiser;
    
    
    private Doctors() {
        
        doctors = new ArrayList<Doctor>();    
        doctorSerialiser = new Serialiser("data//doctors.ser");
    }
    
    
     /**
     * 
     * @return 
     */
    public static Doctors getInstance() {
        // If there is no instance already, create one.
        if (uniqueInstance == null) {
            uniqueInstance = new Doctors();
        }
        
        return uniqueInstance;
    }
    
    
    /**
     * 
     */
    @Override
    public void readData() {
        System.out.println("Reading data in from Doctors.");
        
        // Load the doctors file from disk.
        ArrayList<Doctor> retrievedDoctors = (ArrayList) doctorSerialiser.readObject();
        System.out.println("Retrieved Doctors: " + retrievedDoctors);
        
        if (retrievedDoctors != null) {
            doctors = retrievedDoctors;
            
            // Doctors instance.
            Doctors doctorsInstance = Doctors.getInstance();
            
            // Register all secretaries as observers of this doctors observable.
            Secretaries secretaries = Secretaries.getInstance();
            ArrayList<Secretary> allSecretaries = secretaries.getSecretaries();
            
            for (Secretary secretary: allSecretaries) {
                doctorsInstance.registerUserObserver(secretary);
            }
            
            // Register all administrators as observers of this administrators observable.
            Administrators administrators = Administrators.getInstance();
            ArrayList<Administrator> allAdmins = administrators.getAdministrators();
            
            for (Administrator admin: allAdmins) {
                this.registerUserObserver(admin);
            }
            
            // Register doctors changed observers.
            DoctorsChangeObserver changeObserver = new DoctorsChangeObserver();
            this.registerObserver(changeObserver);
            
            System.out.println("Read in data for Doctors."); 
        } else {
            System.out.println("No initial file, creating new file with Doctors.");
            // If the file does not exist already, create one with a blank array list of administrators.
            saveData();
            
            // Call read data again.
            readData();
        }
    }
    
    
    /**
     * 
     */
    @Override
    public void saveData() {
        // Write the doctors list into the 
        doctorSerialiser.writeObject(doctors);
        System.out.println("Saved data for Doctors.");
    }
    
    /**
     * 
     * @param userLetter
     * @return 
     */
    public String getNextID(String userLetter) {
        // Create a 4 digit integer with leading zeroes in a string.
        String id = userLetter + String.format("%04d", doctors.size() + 1);
        return id;
    }
 
    
    /**
     * 
     * @param doctor
     * @param feedback
     * @return 
     */
    public boolean rateDoctor(Doctor doctor, PatientFeedback feedback) {
        boolean addedRating = false;
        if (doctor != null && feedback != null) {
            // Add doctor feedback and update the doctor in the 
            doctor.addPatientFeedback(feedback);

            // Register the doctor for a notification.
            this.registerUserObserver(doctor);

            // Notify all secretaries and administrators of this change and save.
            this.notifyUserObservers("Doctor Rated - Doctor ID: " + doctor.getID() + " was rated by Patient ID: " + feedback.getUser().getID());
            this.notifyObservers();
            addedRating = true;
        }
        
        return addedRating;
    }
    
    
    /**
     * 
     * @param admin
     * @param doctor
     * @return 
     */
    public boolean createDoctorAccount(Administrator admin, Doctor doctor) {
        boolean addedDoctor = false;
        if (doctor != null) {
            addedDoctor = doctors.add(doctor);
            
            // Register the administrator.
            this.registerUserObserver(admin);
            
            // Notify all secretaries and administrators of this change and save.
            this.notifyUserObservers("Doctor Added - New Doctor ID: " + doctor.getID() + " was created by Admin ID: " + admin.getID());
            this.notifyObservers();
        }
        
        return addedDoctor;
    }
    
    
    /**
     * 
     * @param admin
     * @param doctor
     * @return 
     */
    public boolean removeDoctorAccount(Administrator admin, Doctor doctor) {
        boolean removedDoctor = false;
        if (doctor != null) {
            removedDoctor = doctors.remove(doctor);
            
            // Add the admin as a observer.
            this.registerUserObserver(admin);
            
            // Notify all secretaries and administrators of this change and save.
            this.notifyUserObservers("Doctor Removed - Doctor ID: " + doctor.getID() + " was removed by Admin ID: " + admin.getID());
            this.notifyObservers();
        }
        
        return removedDoctor;
    }
    
    
    /**
     * 
     * @param doctor
     * @param feedback 
     * @return  
     */
    public boolean feedbackToDoctor(Doctor doctor, AdminFeedback feedback) {
        boolean addedFeedback = false;
        if (doctor != null && feedback != null) {
            System.out.println("Checking if doctor is in Doctors list.");
            System.out.println("Doctor is in list.");
            doctor.addAdminFeedback(feedback);

            // Add the doctor as an observer.
            this.registerUserObserver(doctor);

            // Notify all secretaries and administrators of this change and save.
            this.notifyUserObservers("Doctor Admin Feedback  - admin ID: " + feedback.getUser().getID() + 
                    " created feedback for Doctor ID: " + doctor.getID());
            this.notifyObservers();
            addedFeedback = true;
        } else {
            System.out.println("The doctor or the feedback object is null");
        }
        return addedFeedback;
    }
    
    
     /**
     * 
     * @return 
     */
    public ArrayList<Doctor> getDoctors() {
        if (doctors == null) {
            return new ArrayList<Doctor>();
        }
        
        ArrayList<Doctor> temp = new ArrayList<Doctor>();
        
        for (Doctor doctor : doctors) {
            temp.add(doctor);
        }
        return temp;
    }
}
