/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Observers;

/**
 *
 * @author Goel
 */
public interface IObserver {
    
    /**
     * Implements a standard observer interface with a single update method.
     */
    public void update();
}
