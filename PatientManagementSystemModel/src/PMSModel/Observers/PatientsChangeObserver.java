/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Observers;

import PMSModel.Singletons.Patients;

/**
 *
 * @author Goel
 */
public class PatientsChangeObserver implements IObserver {
    private Patients patients = Patients.getInstance();
    
    @Override
    public void update() {
        patients.saveData();
    }
}
