/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Observers;

import PMSModel.Singletons.Secretaries;

/**
 *
 * @author Goel
 */
public class SecretariesChangeObserver implements IObserver {
    private Secretaries secretaries = Secretaries.getInstance();
    
    @Override
    public void update() {
        secretaries.saveData();
    }
}
