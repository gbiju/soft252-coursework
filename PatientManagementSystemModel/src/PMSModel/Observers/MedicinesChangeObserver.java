/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Observers;

import PMSModel.Singletons.Medicines;

/**
 *
 * @author Goel
 */
public class MedicinesChangeObserver implements IObserver {
    
    private Medicines medicines = Medicines.getInstance();
    
    @Override
    public void update() {
        medicines.saveData();
    }
}
