/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Observers;

import PMSModel.Singletons.Doctors;

/**
 *
 * @author Goel
 */
public class DoctorsChangeObserver implements IObserver {

    private Doctors doctors = Doctors.getInstance();
    
    @Override
    public void update() {
        doctors.saveData();
    }
}
