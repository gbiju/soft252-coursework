/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Observers;

import PMSModel.Singletons.Appointments;

/**
 *
 * @author Goel
 */
public class AppointmentsChangeObserver implements IObserver {
    
    private Appointments appointments = Appointments.getInstance();
    
    @Override
    public void update() {
        appointments.saveData();
    }
}
