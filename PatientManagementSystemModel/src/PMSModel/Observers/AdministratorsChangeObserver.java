/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Observers;

import PMSModel.Singletons.Administrators;

/**
 *
 * @author Goel
 */
public class AdministratorsChangeObserver implements IObserver {
    
    private Administrators admins = Administrators.getInstance();
    
    
    @Override
    public void update() {
        admins.saveData();
    }
}
