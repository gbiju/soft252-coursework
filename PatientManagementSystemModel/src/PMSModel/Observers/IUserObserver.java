/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Observers;

/**
 *
 * @author Goel
 */
public interface IUserObserver {
    
    /**
     * The method that should be implemented by any implementing class.
     * Sets the message string which is a notification from an observable.
     * @param message Message text to be encapsulated in a Notification class.
     */
    void update(String message);
}
