/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Observables;

import java.util.ArrayList;
import PMSModel.Observers.IUserObserver;
import PMSModel.Observables.IObservable;
import PMSModel.Observers.IObserver;

/**
 *
 * @author Goel
 */
public class DataObservable implements IObservable {
    
    /**
     *
     */
    protected transient ArrayList<IObserver> observers;

    /**
     *
     */
    protected transient ArrayList<IUserObserver> userObservers;
    
    
    /**
     * 
     * @param observer
     * @return 
     */
    @Override
    public Boolean registerObserver(IObserver observer) {
        Boolean observerAdded = false;
        
        if (observer != null) {
            if (observers == null) {
                observers = new ArrayList<IObserver>();
            }
            
            if (!observers.contains(observer)) {
                observerAdded = observers.add(observer);
            }
        }
        
        return observerAdded;
    }
    
    
    /**
     * 
     * @param observer 
     */
    @Override
    public Boolean registerUserObserver(IUserObserver observer) {
        Boolean observerAdded = false;
        
        if (observer != null) {
            if (userObservers == null) {
                userObservers = new ArrayList<IUserObserver>();
            }
            
            if (!userObservers.contains(observer)) {
                observerAdded = userObservers.add(observer);
            }
        }
        
        return observerAdded;
    }
    
    
    /**
     * 
     * @param observer
     * @return 
     */
    @Override 
    public Boolean removeObserver(IObserver observer) {
        Boolean observerRemoved = false;
        
        if (observer != null) {
            if (this.observers != null && observers.size() > 0) {
                observerRemoved = observers.remove(observer);
            }
        }
        
        return observerRemoved;
    }
    
    
    /**
     * 
     * @param observer 
     */
    @Override 
    public Boolean removeUserObserver(IUserObserver observer) {
        Boolean observerRemoved = false;
        
        if (observer != null) {
            if (this.userObservers != null && userObservers.size() > 0) {
                observerRemoved = userObservers.remove(observer);
            }
        }
        
        return observerRemoved;
    }
    
    
    /**
     * 
     */
    @Override
    public void removeObservers() {
        if (observers != null && observers.size() > 0) {
            observers.clear();
        }
    }
    
    
    /**
     * 
     */
    @Override
    public void removeUserObservers() {
        if (userObservers != null && userObservers.size() > 0) {
            userObservers.clear();
        }
    }
    
    
    /**
     * 
     */
    @Override
    public void notifyObservers() {
        if (observers != null && observers.size() > 0) {
            observers.forEach((observer) -> {
                observer.update();
            });
        }
    }
    
    
    /**
     * 
     * @param message
     */
    @Override
    public void notifyUserObservers(String message) {
        System.out.println("Preparing to send notification to users.");
        
        if (userObservers != null && userObservers.size() > 0) {
            userObservers.forEach((observer) -> {
                System.out.println("Sending message to observer.");
                observer.update(message);
            });
        }
    }
}
