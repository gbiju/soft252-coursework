/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PMSModel.Observables;

import PMSModel.Observers.IObserver;
import PMSModel.Observers.IUserObserver;

/**
 *
 * @author Goel
 */
public interface IObservable {
    
    /**
     * 
     * @param observer
     * @return 
     */
    public Boolean registerObserver(IObserver observer);
   
    
    /**
     * 
     * @param observer 
     * @return  
     */
    public Boolean registerUserObserver(IUserObserver observer);
    
    
    /**
     * 
     * @param observer
     * @return 
     */
    public Boolean removeObserver(IObserver observer);
    
    
    /**
     * 
     * @param observer 
     * @return  
     */
    public Boolean removeUserObserver(IUserObserver observer);
    
    
    /**
     * 
     */
    public void removeObservers();
    
    
    /**
     * 
     */
    public void removeUserObservers();
    
    
    /**
     * 
     */
    public void notifyObservers(); 
    
    
    /**
     * Method to notify observers who are users with a message.
     * @param message
     */
    public void notifyUserObservers(String message);
}
