/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import GUIView.LoginForm;
import PMSModel.Singletons.Administrators;
import PMSModel.Observables.DataObservable;
import PMSModel.Singletons.Doctors;
import PMSModel.Singletons.Patients;
import PMSModel.Singletons.Secretaries;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import PMSModel.Observers.IObserver;

/**
 *
 * @author Goel
 */
public class LoginController implements ActionListener {
    
    // view
    private LoginForm view = null;    
    
    
    public LoginController() {
        
        // Read all data from files from instances.
        Administrators admins = Administrators.getInstance();
        Secretaries secretaries = Secretaries.getInstance();
        Patients patients = Patients.getInstance();
        Doctors doctors = Doctors.getInstance();
        
        admins.readData();
        secretaries.readData();
        patients.readData();
        doctors.readData();
    }
    
    
    public void setView(LoginForm view) {
        this.view = view;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String selectedUserType = view.getSelectedType();
        System.out.println("Got selection: " + selectedUserType);
        if (selectedUserType.equals("Doctor") || selectedUserType.equals("Secretary")) {
            view.setCreateAccountButton(false);
        } else {
            view.setCreateAccountButton(true);
        }
    }
}
